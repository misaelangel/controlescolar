﻿namespace ControlEscolarApp
{
    partial class PrincipalGrupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Visor = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNuevoGrupo = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnEliminarGrupo = new System.Windows.Forms.Button();
            this.gpbGrupos = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Visor)).BeginInit();
            this.gpbGrupos.SuspendLayout();
            this.SuspendLayout();
            // 
            // Visor
            // 
            this.Visor.AllowUserToAddRows = false;
            this.Visor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Visor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Visor.BackgroundColor = System.Drawing.Color.White;
            this.Visor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Visor.GridColor = System.Drawing.Color.White;
            this.Visor.Location = new System.Drawing.Point(19, 64);
            this.Visor.Name = "Visor";
            this.Visor.Size = new System.Drawing.Size(506, 351);
            this.Visor.TabIndex = 0;
            this.Visor.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Visor_CellContentDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Buscar:";
            // 
            // btnNuevoGrupo
            // 
            this.btnNuevoGrupo.BackColor = System.Drawing.Color.Green;
            this.btnNuevoGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNuevoGrupo.FlatAppearance.BorderSize = 0;
            this.btnNuevoGrupo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnNuevoGrupo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnNuevoGrupo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoGrupo.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoGrupo.ForeColor = System.Drawing.Color.Black;
            this.btnNuevoGrupo.Location = new System.Drawing.Point(429, 14);
            this.btnNuevoGrupo.Name = "btnNuevoGrupo";
            this.btnNuevoGrupo.Size = new System.Drawing.Size(33, 33);
            this.btnNuevoGrupo.TabIndex = 15;
            this.btnNuevoGrupo.Text = "+";
            this.btnNuevoGrupo.UseVisualStyleBackColor = false;
            this.btnNuevoGrupo.Click += new System.EventHandler(this.btnNuevoGrupo_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(68, 27);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(317, 20);
            this.txtBuscar.TabIndex = 13;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // btnEliminarGrupo
            // 
            this.btnEliminarGrupo.BackColor = System.Drawing.Color.Red;
            this.btnEliminarGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEliminarGrupo.FlatAppearance.BorderSize = 0;
            this.btnEliminarGrupo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnEliminarGrupo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnEliminarGrupo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarGrupo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarGrupo.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarGrupo.Location = new System.Drawing.Point(478, 14);
            this.btnEliminarGrupo.Name = "btnEliminarGrupo";
            this.btnEliminarGrupo.Size = new System.Drawing.Size(33, 33);
            this.btnEliminarGrupo.TabIndex = 14;
            this.btnEliminarGrupo.Text = "X";
            this.btnEliminarGrupo.UseVisualStyleBackColor = false;
            this.btnEliminarGrupo.Click += new System.EventHandler(this.btnEliminarGrupo_Click);
            // 
            // gpbGrupos
            // 
            this.gpbGrupos.Controls.Add(this.label1);
            this.gpbGrupos.Controls.Add(this.Visor);
            this.gpbGrupos.Controls.Add(this.btnNuevoGrupo);
            this.gpbGrupos.Controls.Add(this.btnEliminarGrupo);
            this.gpbGrupos.Controls.Add(this.txtBuscar);
            this.gpbGrupos.Location = new System.Drawing.Point(15, 6);
            this.gpbGrupos.Name = "gpbGrupos";
            this.gpbGrupos.Size = new System.Drawing.Size(544, 432);
            this.gpbGrupos.TabIndex = 17;
            this.gpbGrupos.TabStop = false;
            // 
            // PrincipalGrupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 450);
            this.Controls.Add(this.gpbGrupos);
            this.Name = "PrincipalGrupos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrincipalGrupos";
            this.Load += new System.EventHandler(this.PrincipalGrupos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Visor)).EndInit();
            this.gpbGrupos.ResumeLayout(false);
            this.gpbGrupos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Visor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnNuevoGrupo;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Button btnEliminarGrupo;
        private System.Windows.Forms.GroupBox gpbGrupos;
    }
}