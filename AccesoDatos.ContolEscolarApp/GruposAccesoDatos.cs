﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Entidades.ControlEscolarApp;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class GruposAccesoDatos
    {
        Conexion _conexion;

        public GruposAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(int idGrupo)
        {
            string cadena = string.Format("Delete from Grupo where idGrupo = {0} ", idGrupo);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Grupo grupo)
        {
            if (grupo.Id == "")
            {
                string cadena = string.Format("insert into Grupo values(null, '{0}', '{1}')", grupo.Nombre, grupo.Carrera);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update Grupo set nombreGrupo = '{0}', fkCarrera = '{1}' where idGrupo = '{2}'", grupo.Nombre, grupo.Carrera, grupo.Id);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public DataSet ObtenerDatos(string filtro)
        {
            string consulta = string.Format("SELECT * FROM Grupo WHERE nombreGrupo LIKE '%{0}%'", filtro);
            return _conexion.ObtenerDatos(consulta, "Grupo");
        }
    }
}
