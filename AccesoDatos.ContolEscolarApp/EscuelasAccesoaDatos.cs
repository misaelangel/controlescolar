﻿using System;
using System.Collections.Generic;
using ConexionBd;
using System.Data;
using Entidades.ControlEscolarApp;
using System.IO;


namespace AccesoDatos.ContolEscolarApp
{
    public class EscuelasAccesoaDatos
    {
        Conexion _conexion;

     public EscuelasAccesoaDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "CONTROLESCOLAR", 3306);
        }

        public void Eliminar(int IdE)
        {
            string cadena = string.Format("Delete from escuela where idescuela = {0} ", IdE);
            _conexion.EjecutarConsulta(cadena);
        }

        public bool Guardar(Escuela escuelas)
        {
            try
            {
                if (escuelas.Idescuela == 0)
                {
                    string cadena = string.Format("insert into escuela values('{0}', '{1}', '{2}', '{3}')", escuelas.Idescuela, escuelas.Nombre, escuelas.Director, /*escuelas.Domicilio, escuelas.Numeroexterior, escuelas.Estado, escuelas.Municipio, escuelas.Telefono, escuelas.E_mail, escuelas.Paginaweb,*/ escuelas.Logo);
                    _conexion.EjecutarConsulta(cadena);
                    return true;
                }
                else
                {
                    string consulta = string.Format("UPDATE ESCUELA SET nombre = '{0}', director = '{1}', logo = '{2}' WHERE idescuela = {3}", escuelas.Nombre, escuelas.Director, escuelas.Logo, escuelas.Idescuela);
                    _conexion.EjecutarConsulta(consulta);
                    return true;
                }
                
            }
            catch (Exception)
            {
                return false;
            }

            
        }

        public DataSet ObtenerLista(string filtro)
        {
            var list = new List<Escuela>();
            string consulta = string.Format("Select * from escuela where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var escuelas = new Escuela
                {
                    Idescuela = Convert.ToInt32(row["idescuela"]),
                    Nombre = row["nombre"].ToString(),
                    Director = row["director"].ToString(),
                    //Domicilio = row["domicilio"].ToString(),
                    //Numeroexterior = row["numeroexterior"].ToString(),
                    //Estado = row["estado"].ToString(),
                    //Municipio = row["municipio"].ToString(),
                    //Telefono = Convert.ToInt32(row["telefono"]),
                    //E_mail = row["e_mail"].ToString(),
                    //Paginaweb = row["paginaweb"].ToString(),
                    Logo = row["Logo"].ToString()
                };
                list.Add(escuelas);
            }
            return ds;
        }

        public bool SavePicture(string sourcePath, string targetPath, string nameFile)
        {
            try
            {
                if (!Directory.Exists(targetPath))
                {
                    Directory.CreateDirectory(targetPath);
                }

                string destFile = Path.Combine(targetPath, nameFile + ".jpg");
                File.Copy(sourcePath, destFile);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Escuela GetEscuela()
        {
            var ds = new DataSet();
            string consulta = "SELECT * FROM escuela";
            ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela = new Escuela();

            foreach (DataRow row in dt.Rows)
            {
                escuela.Idescuela = Convert.ToInt32(row["idescuela"]);
                escuela.Nombre = row["nombre"].ToString();
                escuela.Director = row["director"].ToString();
                escuela.Logo = row["logo"].ToString();
            }

            return escuela;
        }

    }
}
