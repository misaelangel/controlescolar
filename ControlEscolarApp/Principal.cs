﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolarApp;
using LogicaNegocio.ControlEscolarApp;
using System.IO;

namespace ControlEscolarApp
{
    public partial class Principal : Form
    {
        private BackupManejador _backupManejador;
        private Materia _materia;
        public Principal()
        {
            InitializeComponent();
            _materia = new Materia();
            _backupManejador = new BackupManejador();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUsuarios frmUsuarios = new FrmUsuarios();
            frmUsuarios.ShowDialog();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPrincipalAlumnos frmPrincipalAlumnos = new FrmPrincipalAlumnos();
            frmPrincipalAlumnos.ShowDialog();
        }

        private void CatalogosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Principal_Load(object sender, EventArgs e)
        {
            
        }

        private void MaestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrincipalMaestros principalMaestros = new PrincipalMaestros();
            principalMaestros.ShowDialog();
        }

        private void EscuelasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrincipalEscuelas principalEscuelas = new PrincipalEscuelas();
            principalEscuelas.ShowDialog();
        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrincipalMaterias principalMaterias = new PrincipalMaterias();
            principalMaterias.ShowDialog();
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrincipalGrupos principalGrupos = new PrincipalGrupos();
            principalGrupos.ShowDialog();
        }

        private void copiaDeSeguridadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string destino = "";
            string rutaArchivo = "";
            string temporal = @"C:\CEtemp";
            using (var fd = new FolderBrowserDialog())
            {
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fd.SelectedPath))
                {
                    destino = Path.Combine(fd.SelectedPath, "Backup");
                    rutaArchivo = Path.Combine (temporal, "backup.sql");
                }
            }
            if (destino!="")
            {
                if (!Directory.Exists(destino))
                {
                    Directory.CreateDirectory(destino);
                }
                if (!Directory.Exists(temporal))
                {
                    Directory.CreateDirectory(temporal);
                }

                string salida = "Copia de seguridad creada con exito";
                if (!_backupManejador.CopiaSeguridadDatos(@"C:\Logo Escuela\", temporal))
                    salida = "Error al crear la copia de seguridad del logo de la escuela. ";
                if (!_backupManejador.CopiaSeguridadSql(rutaArchivo))
                    salida += "Error al crear la copia de seguridad del logo de la escuela. ";
                if (!_backupManejador.CopiaSeguridadDatos(@"C:\Users\nuevo\", temporal))
                    salida += "Error al crear la copia de seguridad los datos del profesor. ";

                _backupManejador.Comprimir(temporal, destino);

                MessageBox.Show(salida);

            }
            
        }
    }
}
