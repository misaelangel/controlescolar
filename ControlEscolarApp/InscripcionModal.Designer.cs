﻿namespace ControlEscolarApp
{
    partial class InscripcionModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxGruposModal = new System.Windows.Forms.GroupBox();
            this.lblNombreGrupo = new System.Windows.Forms.Label();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtBuscarAlumnoGrupo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarAlumno = new System.Windows.Forms.TextBox();
            this.lblBuscarAlumno = new System.Windows.Forms.Label();
            this.VisorAlumnos = new System.Windows.Forms.DataGridView();
            this.VisorLista = new System.Windows.Forms.DataGridView();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbxGruposModal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisorAlumnos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisorLista)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxGruposModal
            // 
            this.gbxGruposModal.Controls.Add(this.lblNombreGrupo);
            this.gbxGruposModal.Controls.Add(this.btnQuitar);
            this.gbxGruposModal.Controls.Add(this.btnAgregar);
            this.gbxGruposModal.Controls.Add(this.txtBuscarAlumnoGrupo);
            this.gbxGruposModal.Controls.Add(this.label1);
            this.gbxGruposModal.Controls.Add(this.txtBuscarAlumno);
            this.gbxGruposModal.Controls.Add(this.lblBuscarAlumno);
            this.gbxGruposModal.Controls.Add(this.VisorAlumnos);
            this.gbxGruposModal.Controls.Add(this.VisorLista);
            this.gbxGruposModal.Location = new System.Drawing.Point(12, 12);
            this.gbxGruposModal.Name = "gbxGruposModal";
            this.gbxGruposModal.Size = new System.Drawing.Size(768, 452);
            this.gbxGruposModal.TabIndex = 4;
            this.gbxGruposModal.TabStop = false;
            // 
            // lblNombreGrupo
            // 
            this.lblNombreGrupo.AutoSize = true;
            this.lblNombreGrupo.Location = new System.Drawing.Point(521, 16);
            this.lblNombreGrupo.Name = "lblNombreGrupo";
            this.lblNombreGrupo.Size = new System.Drawing.Size(36, 13);
            this.lblNombreGrupo.TabIndex = 8;
            this.lblNombreGrupo.Text = "Grupo";
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(366, 239);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(38, 23);
            this.btnQuitar.TabIndex = 7;
            this.btnQuitar.Text = "<-";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(366, 195);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(38, 23);
            this.btnAgregar.TabIndex = 6;
            this.btnAgregar.Text = "->";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtBuscarAlumnoGrupo
            // 
            this.txtBuscarAlumnoGrupo.Location = new System.Drawing.Point(509, 50);
            this.txtBuscarAlumnoGrupo.Name = "txtBuscarAlumnoGrupo";
            this.txtBuscarAlumnoGrupo.Size = new System.Drawing.Size(243, 20);
            this.txtBuscarAlumnoGrupo.TabIndex = 5;
            this.txtBuscarAlumnoGrupo.TextChanged += new System.EventHandler(this.txtBuscarAlumnoGrupo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Buscar Alumno: ";
            // 
            // txtBuscarAlumno
            // 
            this.txtBuscarAlumno.Location = new System.Drawing.Point(103, 50);
            this.txtBuscarAlumno.Name = "txtBuscarAlumno";
            this.txtBuscarAlumno.Size = new System.Drawing.Size(243, 20);
            this.txtBuscarAlumno.TabIndex = 3;
            this.txtBuscarAlumno.TextChanged += new System.EventHandler(this.txtBuscarAlumno_TextChanged);
            // 
            // lblBuscarAlumno
            // 
            this.lblBuscarAlumno.AutoSize = true;
            this.lblBuscarAlumno.Location = new System.Drawing.Point(13, 53);
            this.lblBuscarAlumno.Name = "lblBuscarAlumno";
            this.lblBuscarAlumno.Size = new System.Drawing.Size(84, 13);
            this.lblBuscarAlumno.TabIndex = 2;
            this.lblBuscarAlumno.Text = "Buscar Alumno: ";
            // 
            // VisorAlumnos
            // 
            this.VisorAlumnos.BackgroundColor = System.Drawing.Color.White;
            this.VisorAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VisorAlumnos.GridColor = System.Drawing.SystemColors.Highlight;
            this.VisorAlumnos.Location = new System.Drawing.Point(16, 78);
            this.VisorAlumnos.Name = "VisorAlumnos";
            this.VisorAlumnos.Size = new System.Drawing.Size(330, 357);
            this.VisorAlumnos.TabIndex = 0;
            // 
            // VisorLista
            // 
            this.VisorLista.AllowUserToAddRows = false;
            this.VisorLista.BackgroundColor = System.Drawing.Color.White;
            this.VisorLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VisorLista.GridColor = System.Drawing.SystemColors.Highlight;
            this.VisorLista.Location = new System.Drawing.Point(422, 78);
            this.VisorLista.Name = "VisorLista";
            this.VisorLista.Size = new System.Drawing.Size(330, 357);
            this.VisorLista.TabIndex = 1;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Red;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Location = new System.Drawing.Point(688, 471);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(76, 27);
            this.btnCancelar.TabIndex = 29;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // InscripcionModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 524);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.gbxGruposModal);
            this.Name = "InscripcionModal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InscripcionModal";
            this.Load += new System.EventHandler(this.InscripcionModal_Load);
            this.gbxGruposModal.ResumeLayout(false);
            this.gbxGruposModal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisorAlumnos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisorLista)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxGruposModal;
        private System.Windows.Forms.TextBox txtBuscarAlumnoGrupo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarAlumno;
        private System.Windows.Forms.Label lblBuscarAlumno;
        private System.Windows.Forms.DataGridView VisorAlumnos;
        private System.Windows.Forms.DataGridView VisorLista;
        private System.Windows.Forms.Label lblNombreGrupo;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnCancelar;
    }
}