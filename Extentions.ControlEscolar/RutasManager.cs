﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Entidades.ControlEscolarApp;


namespace Extensions.ControlEscolar
{
    public class RutaManager
    {
        private string _appPath;

        private const string LOGOS = "logos";


        public RutaManager(string appPath)

        {
            _appPath = appPath;
        }

        public string RutaRepositoriosLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
        }

        public void CrearRepositorioLogos()
        {
            if (!Directory.Exists(RutaRepositoriosLogos))
            {
                Directory.CreateDirectory(RutaRepositoriosLogos);
            }
        }

        public void CrearRepositorioLogosEscuela(int escuelaId)
        {
            //crear los directorios= carpeta + id si no existe y si existe no hace nada y ahi lo guarda
            CrearRepositorioLogos();

            string ruta = Path.Combine(RutaRepositoriosLogos, escuelaId.ToString());
            if (Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }


        //para retornar completamente la ruta completa de la ruta donde esta el ejecutable + el id+ el nombre dellogo que se encuentra en el objeto
        //lo trae de la base de datos

        public string RutaLogoEscuela(Escuela escuelas)
        {
            return Path.Combine(RutaRepositoriosLogos, escuelas.Idescuela.ToString(), escuelas.Logo);
        }
    }
}