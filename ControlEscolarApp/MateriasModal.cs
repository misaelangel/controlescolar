﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class MateriasModal : Form
    {
        private MateriasManejador _materiasManejador;
        private Materia _materia;
        private bool modify;
        public MateriasModal(Materia materia)
        {
            InitializeComponent();
            _materiasManejador = new MateriasManejador();
            _materia = new Materia();

            if (materia == null)
            {
                BuildMateria();
                modify = false;
            }
            else
            {
                _materia = materia;
                BuildEditModal();
                modify = true;
            }
        }

        private void BuildMateria()
        {
            _materia.NombreMateria = txtNombreMateria.Text;
            if (cbxMateriaAnterior.Checked == true)
            {
                _materia.MateriaAnterior = cmbMateriaAnterior.SelectedValue.ToString();
            }
            else
            {
                _materia.MateriaAnterior = "null";
            }
            if (cbxMateriaSiguiente.Checked == true)
            {
                _materia.MateriaSiguiente = cmbMateriaSiguiente.SelectedValue.ToString();
            }
            else
            {
                _materia.MateriaSiguiente = "null";
            }
            if (!modify)
            {
                _materia.IdMateria = "";
            }
        }
        private void BuildEditModal()
        {
            txtNombreMateria.Text = _materia.NombreMateria;
            cmbMateriaAnterior.Text = _materia.MateriaAnterior;
            cmbMateriaSiguiente.Text = _materia.MateriaSiguiente;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            BuildMateria();
            if (ValidarNombreMateria())
            {
                Guardar();
                this.Close();
            }
        }

        private void Guardar()
        {
            _materiasManejador.Guardar(_materia);
        }

        private void MateriasModal_Load(object sender, EventArgs e)
        {
            LoadMaterias();
        }

        private void LoadMaterias()
        {
            var ds1 = _materiasManejador.ObtenerDatos("");
            var ds2 = _materiasManejador.ObtenerDatos("");
            cmbMateriaAnterior.DataSource = ds1.Tables[0].DefaultView;
            cmbMateriaSiguiente.DataSource = ds2.Tables[0].DefaultView;
            cmbMateriaAnterior.ValueMember = "ID";
            cmbMateriaAnterior.DisplayMember = "Nombre";
            cmbMateriaSiguiente.ValueMember = "ID";
            cmbMateriaSiguiente.DisplayMember = "Nombre";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidarNombreMateria()
        {
            var res = _materiasManejador.EsNombreValido(_materia);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }

        private void cbxMateriaAnterior_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxMateriaAnterior.Checked == true)
            {
                cmbMateriaAnterior.Enabled = true;
            }
            else
            {
                cmbMateriaAnterior.Enabled = false;
            }
        }

        private void cbxMateriaSiguiente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxMateriaSiguiente.Checked == true)
            {
                cmbMateriaSiguiente.Enabled = true;
            }
            else
            {
                cmbMateriaSiguiente.Enabled = false;
            }
        }
    }
}
