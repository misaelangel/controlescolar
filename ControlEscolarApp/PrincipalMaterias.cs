﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class PrincipalMaterias : Form
    {
        MateriasManejador _materiasManejador;
        Materia _materia;
        public PrincipalMaterias()
        {
            InitializeComponent();
            _materiasManejador = new MateriasManejador();
            _materia = new Materia();
        }

        private void BuscarMateria(string filtro)
        {
            Visor.DataSource = _materiasManejador.ObtenerDatos(filtro).Tables[0];
            Visor.AutoResizeColumns();
        }

        private void PrincipalMaterias_Load(object sender, EventArgs e)
        {
            BuscarMateria("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMateria(txtBuscar.Text);
        }

        private void btnEliminarMateria_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar esta materia", "Eliminar Materia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    EliminarMateria();
                    BuscarMateria("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarMateria()
        {
            int idMateria = int.Parse(Visor.CurrentRow.Cells[0].Value.ToString());
            _materiasManejador.Eliminar(idMateria);
        }

        private void btnNuevaMateria_Click(object sender, EventArgs e)
        {
            MateriasModal materiasModal = new MateriasModal(null);
            materiasModal.ShowDialog();
            BuscarMateria("");
        }

        private void Visor_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingEscuela();
            MateriasModal materiasModal = new MateriasModal(_materia);
            materiasModal.ShowDialog();
            BuscarMateria("");
        }

        private void BindingEscuela()
        {
            _materia.IdMateria = Visor.CurrentRow.Cells[0].Value.ToString();
            _materia.NombreMateria = Visor.CurrentRow.Cells[1].Value.ToString();
            _materia.MateriaAnterior = Visor.CurrentRow.Cells[2].Value.ToString();
            _materia.MateriaSiguiente = Visor.CurrentRow.Cells[3].Value.ToString();
        }
    }
}
