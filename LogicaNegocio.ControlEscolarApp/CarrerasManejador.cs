﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;

namespace LogicaNegocio.ControlEscolarApp
{
    public class CarrerasManejador
    {
        private CarrerasAccesoDatos _carrerasAccesoDatos;

        public CarrerasManejador()
        {
            _carrerasAccesoDatos = new CarrerasAccesoDatos();
        }
        public DataSet ObtenerCarreras()
        {

            return _carrerasAccesoDatos.ObtenerCarreras();
        }
    }
}
