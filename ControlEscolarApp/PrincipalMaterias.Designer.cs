﻿namespace ControlEscolarApp
{
    partial class PrincipalMaterias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNuevaMateria = new System.Windows.Forms.Button();
            this.btnEliminarMateria = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.Visor = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Visor)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNuevaMateria
            // 
            this.btnNuevaMateria.BackColor = System.Drawing.Color.Green;
            this.btnNuevaMateria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNuevaMateria.FlatAppearance.BorderSize = 0;
            this.btnNuevaMateria.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnNuevaMateria.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnNuevaMateria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevaMateria.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevaMateria.Location = new System.Drawing.Point(657, 11);
            this.btnNuevaMateria.Name = "btnNuevaMateria";
            this.btnNuevaMateria.Size = new System.Drawing.Size(33, 33);
            this.btnNuevaMateria.TabIndex = 11;
            this.btnNuevaMateria.Text = "+";
            this.btnNuevaMateria.UseVisualStyleBackColor = false;
            this.btnNuevaMateria.Click += new System.EventHandler(this.btnNuevaMateria_Click);
            // 
            // btnEliminarMateria
            // 
            this.btnEliminarMateria.BackColor = System.Drawing.Color.Red;
            this.btnEliminarMateria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEliminarMateria.FlatAppearance.BorderSize = 0;
            this.btnEliminarMateria.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnEliminarMateria.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnEliminarMateria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarMateria.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarMateria.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarMateria.Location = new System.Drawing.Point(706, 11);
            this.btnEliminarMateria.Name = "btnEliminarMateria";
            this.btnEliminarMateria.Size = new System.Drawing.Size(32, 33);
            this.btnEliminarMateria.TabIndex = 10;
            this.btnEliminarMateria.Text = "X";
            this.btnEliminarMateria.UseVisualStyleBackColor = false;
            this.btnEliminarMateria.Click += new System.EventHandler(this.btnEliminarMateria_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(67, 24);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(293, 20);
            this.txtBuscar.TabIndex = 9;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // Visor
            // 
            this.Visor.AllowUserToAddRows = false;
            this.Visor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Visor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Visor.BackgroundColor = System.Drawing.Color.White;
            this.Visor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Visor.Location = new System.Drawing.Point(21, 61);
            this.Visor.Name = "Visor";
            this.Visor.ReadOnly = true;
            this.Visor.Size = new System.Drawing.Size(732, 370);
            this.Visor.TabIndex = 8;
            this.Visor.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Visor_CellContentDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Buscar:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Visor);
            this.groupBox1.Controls.Add(this.btnNuevaMateria);
            this.groupBox1.Controls.Add(this.txtBuscar);
            this.groupBox1.Controls.Add(this.btnEliminarMateria);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 449);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // PrincipalMaterias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 473);
            this.Controls.Add(this.groupBox1);
            this.Name = "PrincipalMaterias";
            this.Text = "PrincipalMaterias";
            this.Load += new System.EventHandler(this.PrincipalMaterias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Visor)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNuevaMateria;
        private System.Windows.Forms.Button btnEliminarMateria;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridView Visor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}