﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Entidades.ControlEscolarApp;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class AlumnosInscritosAccesoDatos
    {
        Conexion _conexion;

        public AlumnosInscritosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public DataSet ObtenerAlumnosGrupo(string filtro, string filtro2)
        {
            string consulta = string.Format("SELECT fkAlumno, Nombre, ApellidoP, ApellidoM FROM grupo_alumnos INNER JOIN alumno " +
                "ON alumno.NumeroControl = grupo_alumnos.fkAlumno " +
                "WHERE fkGrupo = '{0}' and Nombre like '%{1}%' or ApellidoP like '%{1}%' or ApellidoM like '%{1}%' or fkAlumno like '%{1}%'", filtro, filtro2);
            return _conexion.ObtenerDatos(consulta, "grupo_alumnos");
        }
        public bool Guardar(AlumnosInscritos alumnoinscrito)
        {
            try
            {
                string cadena = string.Format("insert into grupo_alumnos values({0}, {1})", alumnoinscrito.IdGrupo, alumnoinscrito.NoControl);
                _conexion.EjecutarConsulta(cadena);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string noControl)
        {
            try
            {
                string cadena = string.Format("delete from grupo_alumnos where fkAlumno = '{0}'", noControl);
                _conexion.EjecutarConsulta(cadena);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
