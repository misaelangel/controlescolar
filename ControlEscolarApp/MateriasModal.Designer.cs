﻿namespace ControlEscolarApp
{
    partial class MateriasModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombreMateria = new System.Windows.Forms.TextBox();
            this.lblMateria = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxMateriaSiguiente = new System.Windows.Forms.CheckBox();
            this.cbxMateriaAnterior = new System.Windows.Forms.CheckBox();
            this.lblMateriaSiguiente = new System.Windows.Forms.Label();
            this.cmbMateriaSiguiente = new System.Windows.Forms.ComboBox();
            this.lblMateriaAnterior = new System.Windows.Forms.Label();
            this.cmbMateriaAnterior = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNombreMateria
            // 
            this.txtNombreMateria.Location = new System.Drawing.Point(22, 41);
            this.txtNombreMateria.Name = "txtNombreMateria";
            this.txtNombreMateria.Size = new System.Drawing.Size(291, 20);
            this.txtNombreMateria.TabIndex = 0;
            // 
            // lblMateria
            // 
            this.lblMateria.AutoSize = true;
            this.lblMateria.Location = new System.Drawing.Point(19, 25);
            this.lblMateria.Name = "lblMateria";
            this.lblMateria.Size = new System.Drawing.Size(47, 13);
            this.lblMateria.TabIndex = 1;
            this.lblMateria.Text = "Nombre:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxMateriaSiguiente);
            this.groupBox1.Controls.Add(this.cbxMateriaAnterior);
            this.groupBox1.Controls.Add(this.lblMateriaSiguiente);
            this.groupBox1.Controls.Add(this.cmbMateriaSiguiente);
            this.groupBox1.Controls.Add(this.lblMateriaAnterior);
            this.groupBox1.Controls.Add(this.cmbMateriaAnterior);
            this.groupBox1.Controls.Add(this.lblMateria);
            this.groupBox1.Controls.Add(this.txtNombreMateria);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 210);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // cbxMateriaSiguiente
            // 
            this.cbxMateriaSiguiente.AutoSize = true;
            this.cbxMateriaSiguiente.Location = new System.Drawing.Point(22, 149);
            this.cbxMateriaSiguiente.Name = "cbxMateriaSiguiente";
            this.cbxMateriaSiguiente.Size = new System.Drawing.Size(15, 14);
            this.cbxMateriaSiguiente.TabIndex = 7;
            this.cbxMateriaSiguiente.UseVisualStyleBackColor = true;
            this.cbxMateriaSiguiente.CheckedChanged += new System.EventHandler(this.cbxMateriaSiguiente_CheckedChanged);
            // 
            // cbxMateriaAnterior
            // 
            this.cbxMateriaAnterior.AutoSize = true;
            this.cbxMateriaAnterior.Location = new System.Drawing.Point(22, 96);
            this.cbxMateriaAnterior.Name = "cbxMateriaAnterior";
            this.cbxMateriaAnterior.Size = new System.Drawing.Size(15, 14);
            this.cbxMateriaAnterior.TabIndex = 6;
            this.cbxMateriaAnterior.UseVisualStyleBackColor = true;
            this.cbxMateriaAnterior.CheckedChanged += new System.EventHandler(this.cbxMateriaAnterior_CheckedChanged);
            // 
            // lblMateriaSiguiente
            // 
            this.lblMateriaSiguiente.AutoSize = true;
            this.lblMateriaSiguiente.Location = new System.Drawing.Point(19, 130);
            this.lblMateriaSiguiente.Name = "lblMateriaSiguiente";
            this.lblMateriaSiguiente.Size = new System.Drawing.Size(89, 13);
            this.lblMateriaSiguiente.TabIndex = 5;
            this.lblMateriaSiguiente.Text = "Materia Siguiente";
            // 
            // cmbMateriaSiguiente
            // 
            this.cmbMateriaSiguiente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMateriaSiguiente.Enabled = false;
            this.cmbMateriaSiguiente.FormattingEnabled = true;
            this.cmbMateriaSiguiente.Location = new System.Drawing.Point(43, 146);
            this.cmbMateriaSiguiente.Name = "cmbMateriaSiguiente";
            this.cmbMateriaSiguiente.Size = new System.Drawing.Size(270, 21);
            this.cmbMateriaSiguiente.TabIndex = 4;
            // 
            // lblMateriaAnterior
            // 
            this.lblMateriaAnterior.AutoSize = true;
            this.lblMateriaAnterior.Location = new System.Drawing.Point(19, 77);
            this.lblMateriaAnterior.Name = "lblMateriaAnterior";
            this.lblMateriaAnterior.Size = new System.Drawing.Size(81, 13);
            this.lblMateriaAnterior.TabIndex = 3;
            this.lblMateriaAnterior.Text = "Materia Anterior";
            // 
            // cmbMateriaAnterior
            // 
            this.cmbMateriaAnterior.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMateriaAnterior.Enabled = false;
            this.cmbMateriaAnterior.FormattingEnabled = true;
            this.cmbMateriaAnterior.Location = new System.Drawing.Point(43, 93);
            this.cmbMateriaAnterior.Name = "cmbMateriaAnterior";
            this.cmbMateriaAnterior.Size = new System.Drawing.Size(270, 21);
            this.cmbMateriaAnterior.TabIndex = 2;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Red;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Location = new System.Drawing.Point(171, 244);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(76, 25);
            this.btnCancelar.TabIndex = 25;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSkyBlue;
            this.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(89, 244);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(76, 25);
            this.btnGuardar.TabIndex = 24;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // MateriasModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 281);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox1);
            this.Name = "MateriasModal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nueva Materia";
            this.Load += new System.EventHandler(this.MateriasModal_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombreMateria;
        private System.Windows.Forms.Label lblMateria;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblMateriaAnterior;
        private System.Windows.Forms.ComboBox cmbMateriaAnterior;
        private System.Windows.Forms.Label lblMateriaSiguiente;
        private System.Windows.Forms.ComboBox cmbMateriaSiguiente;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.CheckBox cbxMateriaSiguiente;
        private System.Windows.Forms.CheckBox cbxMateriaAnterior;
    }
}