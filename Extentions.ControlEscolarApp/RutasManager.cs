﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using System.IO;

namespace Extentions.ControlEscolarApp
{
    public class RutasManager
    {
        private string _appPath;
        private const string LOGOS = "logos";

        public RutasManager(string appPath)
        {
            _appPath = appPath;
        }

        public string RutaRepositoriosLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
        }

        public void CrearRepositoriosLogos()
        {
            if (!Directory.Exists(RutaRepositoriosLogos))
                Directory.CreateDirectory(RutaRepositoriosLogos);
            
        }

        public void CrearRepositorioLogosEscuela(int escuelaId)
        {
            CrearRepositoriosLogos();

            string ruta = Path.Combine(RutaRepositoriosLogos, escuelaId.ToString());

            if (Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

        }

        public string RutaLogoEscuela(Escuela escuelas)
        {
            return Path.Combine(RutaRepositoriosLogos, escuelas.Idescuela.ToString(), escuelas.Logo);
        }
    }
}
